Time spent on project
=====================

| Date       | Time spent     |
| ---------- | -------------- |
| 2020-07-29 | 0.9 hours      |
| 2020-07-31 | 1.1 hours      |
| 2020-08-01 | 3.5 hours      |
| 2020-08-05 | 2.5 hours      |
| 2020-08-06 | 0.9 hours      |
| **Total**  | **8.9 hours**  |

[Back to README.md](README.md)
